FROM docker.io/library/nginx:alpine

RUN mkdir /etc/nginx/templates
COPY default.conf.template /etc/nginx/templates/default.conf.template

LABEL org.opencontainers.image.authors="David Rohlicek <david.rohlicek@midacomputers.cz>"